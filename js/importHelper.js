import {GLTFLoader} from '../three.js/examples/jsm/loaders/GLTFLoader.js';
import * as THREE from '../three.js/src/Three.js';
import { Sky } from '../three.js/examples/jsm/objects/Sky.js';

class ImportHelper{
    constructor(manager, width){
        /** cette constante 'self' permet d'accèder aux attributs de notre classe dans des fonctions anonymes. */
        const self = this;

        /** le manager permet de connaitre l'état actuel des loaders rattachés. */
        this.manager = manager;

        /** Largeur de la scène, permet de définir l'échelle de certains composants. */
        this.width = width;

        /** coefficient multiplicateur pour certains composants. */
        this.scaleRatio =  this.width/1000;

        /** Objet contenant le modèle 3D du joueur. */
        this.playerPlane = new THREE.Object3D();

        /** Tableau contenant les différents types d'avions ennemis (modèles 3D) */
        this.planes = [];

        /** Tableau contenant les différents bonus disponibles (modèles 3D) */
        this.bonuses = [];

        /** Tableau contenant les différents types de missiles (modèles 3D) */
        this.missiles = [];

        /** Tableau contenant les material de touches pour l'hud custom. */
        this.hudKeysMaterial = [];

        /** Tableau contenant les material pour les rendu 3D de missiles */
        this.hudRendererMaterial = [];

        /** Mesh contenant le background coucher de soleil. */
        this.firstPersonBackgroundMesh = new THREE.Group();

        /** Material pour le terrain (troisième personne uniquement). */
        this.waterMaterial;

        /** Tableau contenant les différents Meshs rattachés à des TextGeometry. */
        this.textMeshTab = [];

        /** Material pour l'hud custom : sous-module controle clavier */
        this.controlMaterial;

        /** Material pour l'hud custom : sous-module tir de missile */
        this.shootMaterial;

        /** Texture pré-chargée pour les animations. */
        this.fireMaterial;

        /** Tableaux contenant les noms de modèle 3D / images pour chaque type de ressource. */
        this.planesNames = ["plane_niv_1", "plane_niv_2", "plane_niv_3"];
        this.missilesNames = ["missile_niv_1", "missile_niv_2", "missile_niv_3"];
        this.bonusesNames = ["unlimitedAmmo", "addLife"];
        this.hudKeysNames = ["1.png", "2.png", "3.png", "a.png"];
        this.hudRendererNames = ["missile_niv_1_render.png", "missile_niv_2_render.png", "missile_niv_3_render.png"];
        
        /** Objet javascript avec constructeur pour les textMesh */
        this.textMeshOption = function(name, sizeRatio){
            this.name = name;
            this.sizeRatio = sizeRatio;
            return this;
        };

        this.textMeshOptions = [new this.textMeshOption("Déplacements :", 45), 
                                new this.textMeshOption("Tir :", 50), 
                                new this.textMeshOption("Missile 1 :", 55),
                                new this.textMeshOption("Missile 2 :", 55),
                                new this.textMeshOption("Missile 3 :", 55),
                                new this.textMeshOption("Changement de camera :", 70)];

        this.sun_gui_params = {
            inclination: 0.49,
            azimuth: 0.25
        };

        /** A partir d'ici, les différentes variables situées plus haut posséderont un objet (de type Mesh, Material ou Object3D) */
        for(let i = 0 ; i < this.planesNames.length ; i++){
            this.planes.push(new THREE.Object3D());
            this.importGltf(this.planesNames[i], this.scaleRatio, function(object){
                self.planes[i] = object;
            });
        };

        this.playerPlane = new THREE.Object3D();
        this.importGltf("playerPlane", this.scaleRatio, function(object){
            self.playerPlane = object;
        });

        for(let i = 0 ; i < this.missilesNames.length ; i++){
            this.missiles.push(new THREE.Object3D());
            this.importGltf(this.missilesNames[i], this.scaleRatio, function(object){
                self.missiles[i] = object;
            });
        };

        for(let i = 0 ; i < this.bonusesNames.length ; i++){
            this.bonuses.push(new THREE.Object3D());
            this.importGltf(this.bonusesNames[i], this.scaleRatio, function(object){
                self.bonuses[i] = object;
            });
        };

        for(let i = 0 ; i < this.hudKeysNames.length ; i++){
            this.createMaterial(this.hudKeysNames[i],function(material){
                self.hudKeysMaterial[i] = material;
            });
        };

        for(let i = 0 ; i < this.hudRendererNames.length ; i++){
            this.createMaterial(this.hudRendererNames[i],function(material){
                self.hudRendererMaterial[i] = material;
            });
        };

        for(let i = 0 ; i < this.textMeshOptions.length ; i++){
            this.createFont(this.textMeshOptions[i].name, this.width / this.textMeshOptions[i].sizeRatio,function(mesh){
                self.textMeshTab[i] = mesh;
            });
        };

        this.createMaterial("control.png",function(material){
            self.controlMaterial = material;
        });

        this.createMaterial("tir.png",function(material){
            self.shootMaterial = material;
        });

        this.createMaterial("water.jpg",function(material){
            self.waterMaterial = material;
        });

        this.createBackgroundMaterial(this.sun_gui_params, function(group){
            self.firstPersonBackgroundMesh = group;
        });

        this.createFireMaterial(function(material){
            self.fireMaterial = material;
        });
    };

    /** A partir d'ici, nous avons juste les getters pour chaque variable / tableau.  */
    getFirstPersonBackground(){
        return this.firstPersonBackgroundMesh.clone();
    };

    getWaterMaterial(){
        return this.waterMaterial.clone();
    }

    getPlayerPlane(){
        return this.playerPlane.clone();
    };

    getPlane(planeIndex){
        return this.planes[planeIndex].clone();
    };

    getMissile(missileIndex){
        return this.missiles[missileIndex].clone();
    };

    getBonus(bonusIndex){
        return this.bonuses[bonusIndex].clone();
    };

    getHudKeysMaterial(materialIndex){
        return this.hudKeysMaterial[materialIndex].clone();
    };

    getHudRendererMaterial(materialIndex){
        return this.hudRendererMaterial[materialIndex].clone();
    };

    getFontMesh(fontMeshIndex){
        return this.textMeshTab[fontMeshIndex].clone();
    };

    getControlMaterial(){
        return this.controlMaterial.clone();
    };
    
    getShootMaterial(){
        return this.shootMaterial.clone();
    };

    getBackground(){
        return this.firstPersonBackgroundMesh.clone();
    };

    getFireMaterial(){
        /** Remarque : on doit cloner le material ET la texture ici. */
        this.fireMaterial.map =this.fireMaterial.map.clone();
        this.fireMaterial.map.needsUpdate = true;
        return this.fireMaterial.clone();
    };

    /** Methode permettant d'importer un modèle 3D */
    importGltf(name, scaleRatio, _callback){
        let _obj = new THREE.Object3D(); /* Création d'un objet 3D */
        let gltfLoader = new GLTFLoader(this.manager); /** Initialisation du loader gltf */
        gltfLoader.load('./obj/'+name+'.glb', /** Chargement du modèle 3D */
            function ( object ) {
                object.scene.traverse( function( child ) { /** Pour chaque enfant de notre scène 3D... */
                    if ( child.isMesh ) {  /** Si il s'agit d'un mesh */
                        child.castShadow = true; /** Il est capable de projeter une ombre */
                        child.material = new THREE.MeshBasicMaterial({map: child.material.map}); /** On simplifie son type de material pour les performances */
                    }
                });
                _obj.add(object.scene); /** On ajoute la scène 3D à notre objet de destination */
                _obj.castShadow = true; /** on précise que l'objet en question est capable de projeter une ombre également */
                _obj.scale.set(scaleRatio, scaleRatio, scaleRatio); /** Correction d'échelle si nécéssaire */
                _callback(_obj);/** Retour : l'objet */
            }
        );
    };

    /** Methode permettant de créer un material avec gestion de transparence si-nécéssaire. */
    createMaterial(name, _callback){
        let textureLoader = new THREE.TextureLoader(this.manager).load("./textures/" + name, function(texture){
            texture.wrapS = texture.wrapT = THREE.RepeatWrapping;
            texture.repeat.set( 1, 1 );
            let material = new THREE.MeshStandardMaterial({ //Création du material
                map: texture, //Ajout de la texture
                side:THREE.DoubleSide, //Texture mappée uniquement sur la face visible
                transparent:true,
                opacity:1
            });
            _callback(material);
        });
    };

    /** Méthode dérivée de la précédente pour créer un material propre au background première personne. (effet coucher de soleil) */
    createBackgroundMaterial(sun_gui_params, _callback){
        let effectController = {
            turbidity: 10,
            rayleigh: 2,
            mieCoefficient: 0.005,
            mieDirectionalG: 0.8,
            luminance: 1,
            inclination: sun_gui_params.inclination,
            azimuth: sun_gui_params.azimuth,
            sun: ! true
        };
        var distance = 400000;

        let g = new THREE.Group();
        let sky = new Sky();
       
        sky.scale.setScalar( 450000 );
        sky.rotation.x = Math.PI/2;
        sky.rotation.y = Math.PI/2;
        sky.rotation.z = Math.PI/2;
        var uniforms = sky.material.uniforms;

        // Add Sun Helper
        let sunSphere = new THREE.Mesh(
            new THREE.SphereBufferGeometry( 20000, 16, 8 ),
            new THREE.MeshBasicMaterial( { color: 0xffffff } )
        );
        sunSphere.position.y = - 700000;
        
        var theta = Math.PI * ( effectController.inclination - 0.5 );
        var phi = 2 * Math.PI * ( effectController.azimuth - 0.5 );

        sunSphere.position.x = distance * Math.cos( phi );
        sunSphere.position.y = distance * Math.sin( phi ) * Math.sin( theta );
        sunSphere.position.z = distance * Math.sin( phi ) * Math.cos( theta );

        sunSphere.visible = effectController.sun;
        uniforms[ "sunPosition" ].value.copy( sunSphere.position );
        g.add(sky);
        g.add(sunSphere);
        _callback(g)
    };

    /** Création d'un Mesh comprenant une géométrie de type Text.  */
    createFont(text, textSize, _callback){
        var fontLoader = new THREE.FontLoader(this.manager).load('fonts/roboto.json', function(font){
            var geometry = new THREE.TextGeometry(text, {
                font:font,
                size: textSize
            });
            let textMesh = new THREE.Mesh(geometry, new THREE.MeshBasicMaterial({color:0xffffff}));
            textMesh.geometry.computeBoundingBox();
            _callback(textMesh);
        });
    };

    /** Création d'un material pour les animations d'explosions. */
    createFireMaterial(_callback){
        var textureLoader = new THREE.TextureLoader(this.manager).load( "./textures/fire.png", function(texture){ //fichier png contenant les frames d'animation
            texture.wrapS = texture.wrapT = THREE.RepeatWrapping; // Wrapping de texture
            let material = new THREE.MeshBasicMaterial({ //Création du material
                    map: texture,
                    side:THREE.DoubleSide, //Uniquement face-avantpour le mapping
                    transparent:true, //Est transparent
                    opacity:1 //Complètement opaque
            });
            _callback(material);
        });
    };
}
/** Exportation du module importHelper */
export { ImportHelper };